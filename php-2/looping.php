<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Looping</title>
</head>
<body>
    <h1>Berlatih Looping</h1>

    <?php 
        echo "<h3>Soal No 1 Looping I Love PHP</h3>";
        /* 
            Soal No 1 
            Looping I Love PHP
            Lakukan Perulangan (boleh for/while/do while) sebanyak 20 iterasi. Looping terbagi menjadi dua: Looping yang pertama Ascending (meningkat) 
            dan Looping yang ke dua menurun (Descending). 
            
            Output: 
            LOOPING PERTAMA
            2 - I Love PHP
            4 - I Love PHP
            6 - I Love PHP
            8 - I Love PHP
            10 - I Love PHP
            12 - I Love PHP
            14 - I Love PHP
            16 - I Love PHP
            18 - I Love PHP
            20- I Love PHP
            LOOPING KEDUA
            20 - I Love PHP
            18 - I Love PHP
            16 - I Love PHP
            14 - I Love PHP
            12 - I Love PHP
            10 - I Love PHP
            8 - I Love PHP
            6 - I Love PHP
            4 - I Love PHP
            2 - I Love PHP
        */
        // Lakukan Looping Di Sini

        
        for ($i=2; $i <=20; $i+=2) 
        { 
            echo $i.' - I Love PHP<br>';
        }

        for ($b=20; $b >= 2; $b-=2) 
        { 
            echo $b.' - I Love PHP<br>';
        }

        
            
        


        echo "<h3>Soal No 2 Looping Array Modulo </h3>";
        /* 
            Soal No 2
            Looping Array Module
            Carilah sisa bagi dengan angka 5 dari setiap angka pada array berikut.
            Tampung ke dalam array baru bernama $rest 
        */

        $numbers = [18, 45, 29, 61, 47, 34];
        echo "array numbers: ";
        print_r($numbers);
        // Lakukan Looping di sini
        $bilangan = 18; 
        $pembagi = 5; 
        $sisaBagi=$bilangan%$pembagi;
        $hasilBagi=($bilangan-$sisaBagi)/$pembagi;
        

        echo "<br>";
        echo "Array sisa baginya adalah:  $sisaBagi"; 
        echo "<br> ";

        $bilangan2 = 45; 
        $sisaBagi2=$bilangan2%$pembagi;
        $hasilBagi2=($bilangan2-$sisaBagi2)/$pembagi;
        

        echo "<br>";
        echo "Array sisa baginya adalah:  $sisaBagi2"; 
        echo "<br> ";

        $bilangan3 = 29; 
        $sisaBagi3=$bilangan3%$pembagi;
        $hasilBagi3=($bilangan3-$sisaBagi3)/$pembagi;
        

        echo "<br>";
        echo "Array sisa baginya adalah:  $sisaBagi3"; 
        echo "<br> ";

        $bilangan4 = 61; 
        $sisaBagi4=$bilangan4%$pembagi;
        $hasilBagi4=($bilangan4-$sisaBagi4)/$pembagi;
        

        echo "<br>";
        echo "Array sisa baginya adalah:  $sisaBagi4"; 
        echo "<br> ";

        $bilangan5 = 47; 
        $sisaBagi5=$bilangan5%$pembagi;
        $hasilBagi5=($bilangan5-$sisaBagi5)/$pembagi;
        

        echo "<br>";
        echo "Array sisa baginya adalah:  $sisaBagi5"; 
        echo "<br> ";

        $bilangan6 = 34; 
        $sisaBagi6=$bilangan6%$pembagi;
        $hasilBagi6=($bilangan6-$sisaBagi6)/$pembagi;
        

        echo "<br>";
        echo "Array sisa baginya adalah:  $sisaBagi6"; 
        echo "<br> ";

        echo "<h3> Soal No 3 Looping Asociative Array </h3>";
        /* 
            Soal No 3
            Loop Associative Array
            Terdapat data items dalam bentuk array dimensi. Buatlah data tersebut ke dalam bentuk Array Asosiatif. 
            Setiap item memiliki key yaitu : id, name, price, description, source. 
            
            Output: 
            Array ( [id] => 001 [name] => Keyboard Logitek [price] => 60000 [description] => Keyboard yang mantap untuk kantoran [source] => logitek.jpeg ) 
            Array ( [id] => 002 [name] => Keyboard MSI [price] => 300000 [description] => Keyboard gaming MSI mekanik [source] => msi.jpeg ) 
            Array ( [id] => 003 [name] => Mouse Genius [price] => 50000 [description] => Mouse Genius biar lebih pinter [source] => genius.jpeg ) 
            Array ( [id] => 004 [name] => Mouse Jerry [price] => 30000 [description] => Mouse yang disukai kucing [source] => jerry.jpeg ) 

        */
        $items = [
            ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpeg'], 
            ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpeg'],
            ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
            ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpeg']
        ];
        
        foreach ($items as $key => $keys ) {
            echo  $keys[1];
            echo "<br> ";
            echo  $keys[2];
            echo "<br> ";
            echo  $keys[3];
            echo "<br> ";
            echo  $keys[4];
            echo "<br> ";

        }
        // Output: 

        
        
        echo "<h3>Soal No 4 Asterix </h3>";
        /* 
            Soal No 4
            Asterix 5x5
            Tampilkan dengan looping dan echo agar menghasilkan kumpulan bintang dengan pola seperti berikut: 
            Output: 
            * 
            * * 
            * * * 
            * * * * 
            * * * * *
        */
        echo "Asterix: ";
        echo "<br>";     
        $star=5;
        for($a=$star;$a>0;$a--){
        for($b=$star;$b>=$a;$b--){
        echo "*";
        }
        echo "<br>";
        }   
    ?>

</body>
</html>