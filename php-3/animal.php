<?php

class Animal {
  
  public $legs = "4";
  public $name = "Shaun";
  public $cold_blooded = "No";

  public function set_name($name) {
    $this->name = $name;
  }
  public function get_name() {
    return $this->name;
  }
}

?>