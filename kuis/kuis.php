<?php
function tukar_besar_kecil($string){
    
    $maxkata = strlen($string);
    
    for ($x = 0; $x < $maxkata; $x++){
        $kata = $string[$x];
        if (ctype_lower($kata)){
            $huruf_kecil = strtoupper($kata);
            echo $huruf_kecil;
        }
        else{
            $huruf_besar = strtolower($kata);
            echo $huruf_besar;
        }
        
        
    }
    
    echo "<br>";

}

// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>